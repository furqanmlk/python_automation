class parent():

    def launchBrowser(self):
        print("Driving from parent...")

class A(parent):
    
    def myLaunchBrowser(self):
        self.launchBrowser()

    def functionA(self):
        print("I am Class A Function")


class B(parent):
    
    def myLaunchBrowser(self):
        self.launchBrowser()

    def functionB(self):
        print("I am Class B Function")

class C(parent):
    
    def myLaunchBrowser(self):
        self.launchBrowser()

    def functionC(self):
        print("I am Class C Function")

objectA = A()
objectA.launchBrowser()
