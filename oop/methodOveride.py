class parent():

    def launchBrowser(self):
        print("Driving from parent...")
    
    def parentFuncA(self):
        print ("Parent Function A")

class A(parent):

    def functionA(self):
        print("I am Class A Function")

    # Overide method
    def launchBrowser(self):
        print("Driving from Child")

objectA = A()
objectA.launchBrowser()
objectA.parentFuncA()
objectA.functionA()
objectA.launchBrowser()
