
class Person:

    def fName(self,fname):
        print("Fname is %s" %fname)
    
    def lName(self,lname):
        print("Lname is %s" %lname)
    
    def salary(self,fname,lname,salary):
        print("%s %s has salary : %i" %(fname,lname,salary))

class Person2:

    def __init__(self,fName="", lName="", salari="",test="", beting="", fighting=""):
        self.fname = fName
        self.lname = lName
        self.salari = salari
        self.company = "Symantec"
        
    def fName(self):
        print("Fname is %s" %self.fname)
    
    def lName(self):
        print("Lname is %s" %self.lname)
    
    def salary(self):
        print("%s %s has salary : %i" %(self.fname,self.lname,self.salari))  


# Create class object
# personObj = Person()
# personObj.fName("Mike")
# personObj.lName("Bob")
# personObj.salary("Mike","Bob",10000)

personObj = Person2(beting="heloobeting")
personObj.fName()
personObj.lName()
personObj.salary()



