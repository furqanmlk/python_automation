def enumerate_unpakcing():   

    letters = ["a","b","c"]
    for letter in letters:
        print(letter) # 'a','b','c'

    for letter in enumerate(letters):
        print(letter) 
        # (0,'a') 
        # (1,'b') 
        # (2,'c')
        print(letter[0], letter[1])
        # 0 a
        # 1 b
        # 2 c

    # same above result using unpacking
    for index,item in enumerate(letters):
        print(index,item)
        # 0 a
        # 1 b
        # 2 c


def add_remove():
    letters = ["a","b","c","d"]

    # add item to the end
    letters.append("e")

    # add item to specific location
    letters.insert(0,"A")

    # remove item from the end
    letters.pop()

    # remove item from specific location
    letters.pop(0)

    # remove specific item
    letters.remove('A') 
      # Only 1st occurence will be remove
  
    # remove multiple items
    letters = ['a','b','c','d']
    del letters[1:3]  # ['a', 'd']

    # remove all the items from the list
    letters.clear()


def finding_Items():

    letters = ["a","b","c","d"]

    # find how many occurence of item 'd'
    letters.count('d') # 1

    # finding idex of item
    if 'd' in letters:
        print(letters.index('d'))


def sorting_lambda():
    numbers = [3,15,2,8,6]

    # sort changing original list
    numbers.sort()
    print(numbers) # [2, 3, 6, 8, 15]

    # sort descending order
    numbers.sort(reverse=True)
    print(numbers) # [15, 8, 6, 3, 2]

    # Get new sorted list without changing original
    numbers_sordted = sorted(numbers)
    print(numbers) # [3,15,2,8,6]
    print(numbers_sordted) # [2, 3, 6, 8, 15]

    # sort complex list
    items_price = [
        ("prodcut1", 10),
        ("prodcut2", 9),
        ("prodcut3", 12)
    ]
    # items_price.sort() # will not sort

    ### lambda = parameters:expression ###

    # sort using key i.e price 
    items_price.sort(key=lambda item: item[1])
    print(items_price)

    # sort using key i.e product 
    items_price.sort(key=lambda item: item[0])
    print(items_price)


def map_list():

    items_price = [
        ("prodcut1", 10),
        ("prodcut2", 9),
        ("prodcut3", 12)
    ]

    # To get the price list
    prices = []
    for item in items_price:
        prices.append(item[1])
    print(prices) # [10,9,12]
    
    # To get same above result we can use map function
    # map function takes 'func' and 'iterables' as arguments
    # func parameter will execute on each item of iterables
    map(lambda item: item[1], items_price)
    # map will return another iterables
    # let's convert that iterable to list
    prices = list(map(lambda item: item[1], items_price))
    print(prices) # [10,9,12]


def filter_list():
    
    items_price = [
        ("prodcut1", 10),
        ("prodcut2", 9),
        ("prodcut3", 12)
    ]
    # I want to get list of price greater than 9
    # one way is to go throw the list and apply condition
    # other way is to use filter function
    # filter functions take 'func' and 'iterables' as arguments
    # same as map function, 'func' will apply each item of iterable

    filtered = filter(lambda item: item[1] > 9, items_price)
    # filtered will be another iterable
    # let's convert it to list

    filtered_list = list(filtered)
    print(filtered_list) # [('prodcut1', 10), ('prodcut3', 12)]


def list_comprehensions():
    
    items_price = [
        ("prodcut1", 10),
        ("prodcut2", 9),
        ("prodcut3", 12)
    ]
    # We can achieve same result as map and filter function
    # using comprehension
    # [expression for item in items] -> comprehension syntax

    # to get price list from items_price list
    prices = [each_item[1] for each_item in items_price]
    print(prices)

    # list comprehension with condition
    # [expression for each_item in items condition]
    filter_price = [each_item for each_item in items_price if each_item[1] > 9]
    print(filter_price) # [('prodcut1', 10), ('prodcut3', 12)]


def zip_list():
    list1 = [1,2,3]
    list2 = [10,20,30]
   
    # we want to combine above two list like this
    # [(1,10), (2,20), (3, 30) ]

    # map and filter function works only on single list

    # zip function takes two or more iterable and combine
    zipped = zip(list1,list2)
    print(list(zipped)) # [(1, 10), (2, 20), (3, 30)]

    list1 = [1,2,3]
    list2 = ['a','b']
    zipped = zip(list1,list2)
    print(list(zipped)) # [(1, 'a'), (2, 'b')]