# We use array if we are dealing with large number of data
# and we are having performance issue

from array import array

# How to create array
# array takes two param, typecode, list of items
# typecode of int is 'i'
# you can see typecode by typing "array( + ctrl + space"
numbers = array("i", [1,2,3])
numbers.append(4)
numbers.pop(4)

# We can do all sort of method on array as list
# it is just used for performance improvment