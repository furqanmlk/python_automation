def tuple_basics():
    # How define tuple
    point = (1,2)
    point = 1,2  # Tuple without brackets
    point = 1,  # Tuple with one value

    # Tuple contcatinate
    point = (1,2) + (3,4) # (1, 2, 2, 3)

    # Tuple with multiply
    point = (1,2) * 3 # (1, 2, 1, 2, 1, 2)

    # Convert list to Tuple
    point = tuple([1,2]) # (1, 2)

    # Convert string to Tuple
    point = tuple("Hello Hi") 
    # ('H', 'e', 'l', 'l', 'o', ' ', 'H', 'i')

    # Accessing tuple item
    point = 1,2,3,4
    point[0] # 1
    point[0:2] # (1,2)

    # unpacking tuple items
    point = 1,2,3
    x,y,z = point
    # x = 1 , y = 2, z = 3


def swapping_variables():

    # Old way
    x,y = 10,12
    z = x
    x = y
    y = z

    # or
    z,x,y = x,y,z # x = 20, y = 10

    # Python way
    x,y = y,x   # x = 20, y = 10

    # what's going on in above statement
    x,y = (y,x)
    # or
    x,y = (10,12) # Remember unpacking
    