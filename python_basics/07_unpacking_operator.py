# I would like to print list items in normal form not in list formate 
numbers = [1,2,3]
# print(numbers) will print [1,2,3]
# I want to print 1 2 3

# we use unpacking operator '*' with variable
print(*numbers) # 1 2 3

# list if numbers
values = list(range(5))

# What if I want list of num and string
values = [*range(5), *"Hello"]
print(values) # [0, 1, 2, 3, 4, 'H', 'e', 'l', 'l', 'o']

# We can also combine two list
first = [1,2,3]
second = ["a","b"]
values = [*first, *"HI", *second, *"E"]
print(values) # [1, 2, 3, 'H', 'I', 'a', 'b', 'E']

# Unpacking operator can be used for Dictionary
first = dict(x=1,w=5)
second = dict(x=10,y=2)
combined = {**first, **second, 'z':11} # {'x': 10, 'w': 5, 'y': 2, 'z': 11}
# Notice that last value of X was taken
