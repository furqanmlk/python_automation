# Sets is unordered collection of unique numbers
# Does not support indexing, because it is not in ordered

numbers = [1,2,2,3,3,4]
uniques = set(numbers) # {1,2,3,4}
# notice set uses curly brackets

myset = {1,4} # set is created
myset.add(5) # {1,4,5}
myset.remove(5) # {1,4}
len(myset) # 2

# Set works good for meth operation
first = {1,2,3,4}
second = {1,5}

# Union of two sets, all unique items from both sets
first | second  # {1,2,3,4,5}

# AND operation, only common items
first & second # {1}

# Difference operation, take items of second set from first set and print first set
first - second # {2,3,4} 

# carrot operation, ignore duplicate from both sets
# 1 is duplicate in both sets
first ^ second # {2,3,4,5}



