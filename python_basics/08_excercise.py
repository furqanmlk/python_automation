def count_char_in_sentence():
    # Find the most repeated letter in following line
    
    sentence = "This is a common interview question"
    
    letterDic = {}
    
    for e in sentence:
        if e in letterDic and e != " ":
            letterDic[e] += 1
        else:
            letterDic[e] = 1
    
    # OLD WAY TO FIND MAX / MIN VALUE FROM DICTIONARY
        # max = 0
        # letter = ""
        # for k,v in letterDic.items():
        #     if v > max:
        #         letter = k
        #         max = v
        # print(letter,max)
    
    # Another way to find max value in dictionary
    # Instead of looping through dictionary, use lambda
    
    # This is line will sort the dictionary using key, but we want using value
    sortedDic = sorted(letterDic.items()) 
    # Output : [('a', 1), ('b', 14), ('x', 5), ('y', 8)]
    
    # Remember: lambda --> paramter:expresion (i.e. expression will go through all dic items)
    sortedDic = sorted(letterDic.items(),key=lambda kv:kv[1])
    # Output: [('a', 1), ('x', 5), ('y', 8), ('b', 14)] 
    
    sortedDic = sorted(letterDic.items(),key=lambda kv:kv[1],reverse=True)
    # Output: [('b', 14), ('y', 8), ('x', 5), ('a', 1)]
    
    sortedDic[0] # ('b',14)


def fizz_buzz(number):
    if (number % 3) == 0 and number % 5 == 0:
        return "FizzBuzz"
    elif number % 5 == 0:
        return "Fizz"
    elif number % 3 == 0:
        return "Buzz"
    return number

print(fizz_buzz(17))


def remove_dup_balance_two_lists():
    # Remove duplicate from basket 2 and balance fruits in both basckets
    basket1 = ["apple","banana","mango","cherry","cucumber"]
    basket2 = ["carrot","orange","mango"]
    
    # remove dublicate item from basket2
    for item in basket1:
        if item in basket2:
            basket2.remove(item)
    
    print(f"Basket 1 : {basket1}")
    print(f"Basket 2 : {basket2}")
    
    # Balancing fruits in both baskets
    while len(basket1)-1 >= len(basket2)-1:
        transfer_item = basket1.pop()
        basket2.append(transfer_item)
        print(f"Basket 1 len: {len(basket1)}")
        print(f"Basket 2 len: {len(basket2)}")
    
    print(f"Basket 1 : {basket1}")
    print(f"Basket 2 : {basket2}")
