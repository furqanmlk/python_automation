# It is the collection of key value pair
# phone book is dictionaries
# name is key, phone number is value
# Khan -> 22-33-44-555

def dictionary_basics():
    # How define Dictionary
    book = {"x": 1, "y": 2}

    # or
    book = dict(x=1, y=2)  # Prefered
    # Notice, no quote with key and there is equal sign
    # easy to generate Dictionary

    # Accessing item using key
    book['x']  # 1

    # updating value of key
    book['y'] = 4

    # adding new key value
    book['z'] = 5

    print(book)  # {'x': 1, 'y': 4, 'z': 5}

    # accessing non exsting key
    book['a']  # throws KeyError

    # to avoid keyError, use get
    book.get('a')  # return None, no error

    # deleting dictionary item using key
    del book['x']
    print(book)  # {'y': 4, 'z': 5}

    # loop throw dictionary items

    # by default for loop uses key
    for key in book:
        print(key)  # y, z
        print(key, book[key])  # y 4, z 5

    # getting key value in loop
    for item in book.items():
        print(item)  # ('y', 4) ('z', 5)

    for key, value in book.items():  # Unpacking
        print(key, value)  # y 4, z 5


def dictionary_comprehensions():

    # let's review list comprehension
    # Normal way 
    values = []
    for x in range(5):
        values.append(x)
    print(values) # [0, 1, 2, 3, 4]

    # List comprehension
    values = [x for x in range(5)]
    print(values) # [0, 1, 2, 3, 4]

    # Set comprehension
    values = {x for x in range(5)}
    print(values) # {0, 1, 2, 3, 4}

    # Both Set and Dict use curly brackets
    # but set use values only, dict uses key value

    # Dict normal way
    values = {}
    for x in range(5):
        values[f"key{x}"] = x * 2

    print(values) # {'key0': 0, 'key1': 2, 'key2': 4, 'key3': 6, 'key4': 8}

    # Dict Comprehension
    values = {f"key{x}": x*2 for x in range(5)}
    print(values) # {'key0': 0, 'key1': 2, 'key2': 4, 'key3': 6, 'key4': 8}


    # IF we try comprehension for tuple
    # we will get generator object
    values = (x for x in range(5))
    print(values) # <generator object <genexpr> at 0x7efc2dd53a40>

    # See Generator topic :)