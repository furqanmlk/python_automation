from sys import getsizeof
# let's consider an example
values = [x*2 for x in range(1000)]
print(getsizeof(values))  # 9024

values = [x*2 for x in range(100000)]
print(getsizeof(values))  # 824464

for x in values:
    print(x)
# When you are working with large numbers
# storing range in variable is not a good practice
# like above, 'values' variable will not be memory effecient

# Generator is used for this purpose
# In above example, 'values' variable is list

# Here values variable is generator object
values = (x*2 for x in range(1000)) # Notice round bracket
print(getsizeof(values))  # 88

values = (x*2 for x in range(100000)) # Notice round bracket
print(getsizeof(values))  # 88 :)

for i in values:
    print(values)

# Result is still the same, but notice the size of 'values' variable

# Remember, len function will throw error for generator object