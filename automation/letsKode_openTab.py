from selenium import webdriver
import time

def openTab():
    firefoxDriver = webdriver.Firefox(executable_path = "automation/geckodriver")

    firefoxDriver.get("https://learn.letskodeit.com/p/practice")

    openTabButton = firefoxDriver.find_element_by_xpath("//a[@id='opentab'][text()='Open Tab']")

    openTabButton.click()

    return True

