from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import login

def createCustomer(firefoxDriver):
    # Click on Add Customer Button to open New Customer window dialog
    addCustoerButton=firefoxDriver.find_element_by_xpath("//span[contains(@id,'-btnInnerEl') and text()='Add Customer']")
    addCustoerButton.click()
    time.sleep(2)

    # Fill all required fields
    companyName = firefoxDriver.find_element_by_xpath("//input[@name='name']")
    #time.sleep(2)    
    companyName.send_keys("Amazing-Waterloo")

    
    subscriptionID = firefoxDriver.find_element_by_xpath("//input[@name='subscriptionId']")
    #time.sleep(2)
    subscriptionID.send_keys("Amazing-Waterloo")

    addressLine1 = firefoxDriver.find_element_by_xpath("//input[@name='address.address1']")
    #time.sleep(2)
    addressLine1.send_keys("209 Frobisher Drive")

    addressLine2 = firefoxDriver.find_element_by_xpath("//input[@name='address.address2']")
    #time.sleep(2)
    addressLine2.send_keys("Waterloo-Ontario")

    zipPostalcode = firefoxDriver.find_element_by_xpath("//input[@name='address.zip']")
    #time.sleep(2)
    zipPostalcode.send_keys("N2V 2G4")

    countryName = firefoxDriver.find_element_by_xpath("//input[@name='address.country']")
    #time.sleep(2)
    countryName.send_keys("Canada")

    # Add button
    addButton = firefoxDriver.find_element_by_xpath("//span[contains(@id,'-btnInnerEl') and text()='Add']")
    #time.sleep(2)
    addButton.click()
    
    return

def isCustomerExist(firefoxDriver):

    searchCustomer=firefoxDriver.find_element_by_xpath("//input[@name='bc-advSearchField-1196-inputEl']")
    searchCustomer.send_keys("Amazing-Waterloo")
    time.sleep(2)

    startSearch=firefoxDriver.find_element_by_xpath("//div[@id='bc-advSearchField-1196-trigger-picker']")
    time.sleep(2)
    startSearch.click()

    try:
        firefoxDriver.find_element_by_xpath("//div[contains(@id,'tbtext') and text()='Displaying customers 1 - 1 of 1']")
        # open Product window
        
        return True
    except :
        createCustomer(firefoxDriver)

def addCustomer():

    #==============================================================
    # Login scrip is called 
    #===============================================================

    firefoxDriver = login.loginPortal()

    #==============================================================
    # After login to Opt portal Add Customer button will be clicked
    # Customer details will be provided in New Customer window
    # And customer will be added to the customers list
    #===============================================================
    
    isCustomerExist(firefoxDriver)
    

    # Add WEB_SECURITY as a Product
    addWebSecurity = firefoxDriver.find_element_by_xpath("//div[@class='x-grid-item-container']//div[starts-with(text(),'WEB_SECURITY')]/a")
    addWebSecurity.location_once_scrolled_into_view
    addWebSecurity.click()
    time.sleep(2)
    

    radioButton= firefoxDriver.find_element_by_xpath("//span[@id='radio-1270-displayEl']")
    time.sleep(1)
    radioButton.click()
    time.sleep(1)

    numberOfSeats = firefoxDriver.find_element_by_xpath("//input[@name='numberOfSeats']")
    numberOfSeats.send_keys("50")
    time.sleep(2)

    serialNumber = firefoxDriver.find_element_by_xpath("//input[@name='serialNumber']")
    serialNumber.send_keys("3xcus3m3")
    time.sleep(2)
    
    # Close button
    addButton = firefoxDriver.find_element_by_xpath("//span[contains(@id,'-btnInnerEl') and text()='Add']")
    time.sleep(2)
    addButton.click()


addCustomer()