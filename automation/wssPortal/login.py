from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

def loginPortal():

    firefoxDriver = webdriver.Firefox(executable_path = "automation/geckodriver")

    firefoxDriver.get("http://10.169.25.98:8080/login.jsp")

    firefoxDriver.implicitly_wait(5)

    userName = firefoxDriver.find_element_by_xpath("//input[@name='username']")
    
    # userName.send_keys("tesing",Keys.ENTER)   # this code is to press ENTER after send key
    userName.send_keys("cloud-admin@bluecoat.com")
    
    # Login Button
    nextButton = firefoxDriver.find_element_by_xpath("//a[@id='button-1040']")
    nextButton.click()

    # Password Field
    passwordField = firefoxDriver.find_element_by_xpath("//input[@id='textfield-1042-inputEl']")
    passwordField.send_keys("Admin123")


    # sing-in button
    signInButton = firefoxDriver.find_element_by_xpath("//span[@id='button-1046-btnInnerEl']")
    signInButton.click()


    # Verify logged in screen
    try:
        labelText = firefoxDriver.find_element_by_xpath("//label[@id='label-1017']")
        return firefoxDriver
    except:
        firefoxDriver.close()
        return False
    
# print (loginPortal())