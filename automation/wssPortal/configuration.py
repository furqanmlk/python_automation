from selenium import webdriver

import time

import login

def configuration():
    firefoxDriver = login.loginPortal()

    # Click on configuration tab
    configurationButton = firefoxDriver.find_element_by_xpath("//span[contains(@id,'tab-1130-btnInnerEl') and text()='Configuration']")
    configurationButton.click()
    
    return firefoxDriver
