from selenium import webdriver
import time

def scrollToView():
    firefoxDriver = webdriver.Firefox(executable_path = "automation/geckodriver")

    firefoxDriver.get("https://learn.letskodeit.com/p/practice")

    seachButton = firefoxDriver.find_element_by_xpath("//i[@class='fa fa-search']")
    seachButton.location_once_scrolled_into_view
    seachButton.click()
    time.sleep(5)

    return True

scrollToView()