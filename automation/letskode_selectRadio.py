from selenium import webdriver
import time

def radioButton():
    firefoxDriver = webdriver.Firefox(executable_path = "automation/geckodriver")

    firefoxDriver.get("https://learn.letskodeit.com/p/practice")

    bmwRadio = firefoxDriver.find_element_by_xpath("//input[@id='bmwradio']")

    bmwRadio.click()

    crntWind = firefoxDriver.window_handles[1]
    firefoxDriver.switch_to.window(crntWind)
    

    return bmwRadio.is_selected()


