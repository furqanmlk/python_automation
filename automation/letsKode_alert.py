from selenium import webdriver
import time

def alertBox():
    firefoxDriver = webdriver.Firefox(executable_path = "automation/geckodriver")

    firefoxDriver.get("https://learn.letskodeit.com/p/practice")

    alertButton = firefoxDriver.find_element_by_xpath("//input[@id='alertbtn']")

    alertButton.click()
    time.sleep(5)
    alert = firefoxDriver.switch_to.alert
    alert.accept()

    firefoxDriver.close()
