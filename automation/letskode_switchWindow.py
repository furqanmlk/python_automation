from selenium import webdriver
import time

def switchWindow():
    firefoxDriver = webdriver.Firefox(executable_path = "automation/geckodriver")

    firefoxDriver.get("https://learn.letskodeit.com/p/practice")

    switchWinButton  = firefoxDriver.find_element_by_xpath("//button[@id='openwindow']")

    switchWinButton.click()

    # Getting handles of all open widnows
    allwinds = firefoxDriver.window_handles
    # Switching to second window
    firefoxDriver.switch_to.window(allwinds[1])

    time.sleep(5)
    searchCourseField  = firefoxDriver.find_element_by_xpath("//input[@id='search-courses']")
    searchCourseField.send_keys("Python")
    searchCourseField.submit()

#switchWindow()