from selenium import webdriver
import time

def login():
    firefoxDriver = webdriver.Firefox(executable_path = "automation/geckodriver")

    firefoxDriver.get("https://learn.letskodeit.com/p/practice")

    loginButton = firefoxDriver.find_element_by_xpath("//div[@id='navbar']//a[contains(text(),'Login')]")

    loginButton.click()

    # Verifying if login page is open
    try:
        textMesg = firefoxDriver.find_element_by_xpath("//h1[@class='text-center' and contains(text(),'Log In to Let')]")
    except:
        firefoxDriver.close()
        return False
    
    firefoxDriver.close()
    return True

#login()



