import unittest
import automation.letsKode_login as login
import automation.letsKode_openTab as tab
import automation.letskode_selectRadio as radio

class TestLetsKode(unittest.TestCase):

    def test_login(self):
        status = login.login()
        self.assertTrue(status)

    def test_openTab(self):
        status = tab.openTab()
        self.assertTrue(status)

    def test_radioButton(self):
        status = radio.radioButton()
        self.assertTrue(status)

