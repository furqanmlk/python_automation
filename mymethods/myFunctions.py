def functionNoParam():
    print("Function A")

def functionString(name):
    print("Hello dear %s" %name)


def functionDefaultValue(company = "Symantec"):
    print("Well come " + company)
    

# Method Overloading
def functionString(fname,lname):
    print ("Well come %s %s" %(fname,lname))

functionNoParam()

functionString("Mike")

functionDefaultValue()
